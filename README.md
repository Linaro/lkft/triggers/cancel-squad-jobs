# Description
This repository includes pipeline files for displaying and cancelling tuxplans
and lava test jobs that are currently submitted and active.

The script offers options to display and cancel jobs that are a specified
number of DAYS old for a particular GROUP and PROJECT.

## Variables
Variables which require to be setup in CI/CD -> variable is 'SQUAD_TOKEN',
'TUXSUITE_TOKEN', 'TUXSUITE_GROUP' and 'TUXSUITE_PROJECT'.
