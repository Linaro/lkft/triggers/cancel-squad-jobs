#!/bin/bash
# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2022-present Linaro Limited
#
# SPDX-License-Identifier: MIT

limit=500
days=15
cancel="False"

usage() {
    echo "Usage: ${0} [-d days][-l limit][-h]
          -c,       cancel jobs
          -d days,  default 15
          -l limit, default 500
          -h,       This help  text"
}

while getopts "hcd:l:" arg; do
    case "$arg" in
        c)
            cancel="True"
            ;;
        d)
            days=$(("$OPTARG"))
            ;;
        l)
            limit=$(("$OPTARG"))
            ;;
        h|*)
            usage
            exit 0
            ;;
    esac
done

tuxsuite plan list --json-out /tmp/test-plan.json --limit ${limit}
indate=$(date --date="-d - ${days} days" +"%Y-%m-%dT%H:%M:%SZ")

for uid in $(jq --arg date "$indate" '.[] | select(.provisioning_time < $date and .extra.state != "finished") | .uid' /tmp/test-plan.json |sed -e 's|\"||g'); do
    if [[ ${cancel} == "True" ]]; then
        tuxsuite plan cancel $uid
        echo -n "Cancel plan id: "
    fi
    echo $uid
done
